# Rules

- Adhere to our Community Code of Conduct (https://linux.social/about).
- Use welcoming, inclusive and friendly language.
- Be respectful of differing viewpoints and experiences.
- Gracefully accept constructive criticism and give it - where needed - only respectfully.
- Show empathy towards other community members and focus on what is best for the community.
- No sexualized language, imagery, or sexual attention/advances.
- No trolling, insulting/derogatory comments, or personal/political/religious attacks.
- No public or private harassment.
- Do not publish others' private information, such as a physical or electronic addresses, without explicit permission.
- Do not engage in conduct which could reasonably be considered inappropriate in a professional setting.
- No posting of commercial or business content (unless sharing your Linux-related activities or experiences).
- No posting or boosting the same content repeatedly or frequently.
- No botting or cross-posting from other services if those posts include jargon or links to those services.
- Avoid political and religious topics, there are instances better suited to these topics.
- A reasonable ratio of posts must contain Linux®-related content.
- Refer to our Privacy Policy (https://linux.social/privacy-policy) regarding minimum age requirements for using this server.

